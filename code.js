var array=[]
var a="abc";
var b = "normal";
var c = "2/10/17"
array.push( {"Task":a, "Cat":b, "Due":c } )
console.log(array);

// --------Array Object Sort 
var items = [
  { name: 'Edward', value: 21 },
  { name: 'Sharpe', value: 37 },
  { name: 'And', value: 45 },
  { name: 'The', value: -12 },
  { name: 'Magnetic', value: 13 },
  { name: 'Zeros', value: 37 }
];


// ------------------------Start of Sorting Function---------------------------------------------------
// sort by name
items.sort(function(a, b) {
  var nameA = a.name.toUpperCase(); // ignore upper and lowercase
  var nameB = b.name.toUpperCase(); // ignore upper and lowercase
  if (nameA < nameB) {
    return -1;
  }
  if (nameA > nameB) {
    return 1;
  }

  // names must be equal
  return 0;
});

console.log(items);

// sort by value
items.sort(function (a, b) {
  return a.value - b.value;
});

// ===========================End of Sorting function==================================================

// ---------------------------Start of Delete function ------------------
// to delete record in the Todo list
<div ng-app="appTable">
                        <h3>To Remove Task</h3>
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <td>Task</td>
                                    <td>Date</td>
                                    <td>Cat</td>
                                    <td>Done</td>
                                    <td> </td>
                                </tr>
                            </thead>
                            <tbody>                   
                                <tr ng-repeat="todo in firstCtrl.result track by $index" >
                                    <td>{{todo.Task}}</td>
                                    <td>{{todo.Date | date}}</td> 
                                    <td>{{todo.Cat}}</td>
                                    <td>{{todo.Done}}</td> 
                                    <td>
                                        <button type="button" class="btn btn-danger btn-sm" ng-click="firstCtrl.removeRow($index)"><span class="glyphicon glyphicon-remove"></span></button>
                                    </td>
                                    <!--<td> <input type="button" value="X" class="btn btn-primary btn-sm" ng-click="firstCtrl.removeRow($index)"> </td>-->
                                </tr>
                            </tbody>
                        </table>
                </div>

// =======================end of delete function=================================================
