//for client side we use IIFE to execute the application

(function() {
//Everything inside this function is our application
//Create an instance of Angular applcation/module
// 1st param: app's name, this is the apps dependencies

var FirstApp = angular.module("FirstApp", []);

var reverse = function(s){
    return s.split("").reverse().join("");
}

var FirstCtrl = function() {
    //Hold a reference of this controller so that when 'this' changes we are 
    //still referncing the controller
    var firstCtrl = this;
    console.log("Hello");

    //Define first model of this controller/vm 
    firstCtrl.myText1 =0;
    firstCtrl.myText2 =0;
    firstCtrl.err="";
    // define another model 
    firstCtrl.result = "";

    // define an event for the click button
  
    firstCtrl.validateInputs = function() {
        x = firstCtrl.myText1;
        y = firstCtrl.myText2;
        if ( (!isNaN(x)) &&  (!isNaN(y)) ) {
            return true;
        } else {
            firstCtrl.err = "Not a number";
            return false;
        }
    }

    firstCtrl.clear = function(){
        firstCtrl.result = "";
        firstCtrl.err ="";
    }

    
    firstCtrl.add = function(){
        firstCtrl.clear();
        if (firstCtrl.validateInputs())
            firstCtrl.result = parseFloat(firstCtrl.myText1) + parseFloat(firstCtrl.myText2);
        }
    firstCtrl.sub = function(){
       firstCtrl.clear();
       
        if (firstCtrl.validateInputs())
            firstCtrl.result = parseFloat(firstCtrl.myText1) - parseFloat(firstCtrl.myText2);
        }
    firstCtrl.div = function(){
       firstCtrl.clear();

        if (firstCtrl.validateInputs())
            firstCtrl.result = parseFloat(firstCtrl.myText1) / parseFloat(firstCtrl.myText2);
        }
    firstCtrl.mul = function(){
        firstCtrl.clear();
        
        if (firstCtrl.validateInputs())
            firstCtrl.result = parseFloat(firstCtrl.myText1) * parseFloat(firstCtrl.myText2);
        }
    // define an event for the clear text
    firstCtrl.clearText = function(){
        firstCtrl.reverseMyText ="";    
        }
    } 
   
       
  //  }

//}

//Define a controller call firstCtrl
FirstApp.controller("FirstCtrl", [FirstCtrl])
})();

