//for client side we use IIFE to execute the application

(function() {
//Everything inside this function is our application
//Create an instance of Angular applcation/module
// 1st param: app's name, this is the apps dependencies

var MyApp = angular.module("MyApp", []);

var reverse = function(s){
    return s.split("").reverse().join("");
}

var MyCtrl = function() {
    //Hold a reference of this controller so that when 'this' changes we are 
    //still referncing the controller
    var firstCtrl = this;
    console.log("Hello");

    //Define first model of this controller/vm 
    firstCtrl.myText ="hello, world";

    // define another model 
    firstCtrl.reverseMyText = "";

    // define an event for the click button
    firstCtrl.reverseText = function(){
        firstCtrl.reverseMyText = reverse(firstCtrl.myText);
    }

    // define an event for the clear text
    firstCtrl.clearText = function(){
        firstCtrl.reverseMyText ="";    
    }
}

//Define a controller call firstCtrl
MyApp.controller("FirstCtrl", [MyCtrl])
})();

